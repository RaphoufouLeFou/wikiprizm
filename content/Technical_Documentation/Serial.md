---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-10T21:58:24Z'
title: Serial
---

The Prizm's 2.5mm link port is a standard UART, capable of operating at
up to 115200 bits per second in normal configurations. See the [serial
syscalls]({{< ref "Syscalls/Serial/" >}}) for usage information in
add-ins.

## Connector and Cable {#connector_and_cable}

The connectors on the link cable are standard 2.5 mm phone connectors
with three pins. The cables are passive, free of electronic components,
and arranged so that the transmission pin on one end connects to the
receive pin on the other end (crossover).

## Pinout

From the male connector view, the sleeve is ground, the ring is the
receive pin and the tip is the transmit pin.

## Operating voltage {#operating_voltage}

The voltage level +4.2V is used for logical 1, and 0V for logical 0.

## Supported settings {#supported_settings}

Through the use of [serial syscalls]({{< ref "Syscalls/Serial/" >}}),
software can operate the serial port at ten different baud rates, from
300 baud to 115200 baud, using odd, even or no parity, one or two stop
bits and seven or eight bits of data length - refer to the [parameter
description of
Serial_Open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}) for more
information. By fiddling with the serial port registers, the serial port
can be used in a more direct fashion, including higher speeds, at which
reliable data transfer might not be possible.

## Addresses

The 3-pin port corresponds to the first serial communication interface
of the SH7305, SCIF0. Its base address is 0xA4410000 and it is identical
to the one found in the SH7724 and in the SH7730.

## Overclocking

When the system
[PLL]({{< ref "Technical_Documentation/CPU_Clocks.md" >}}) is reclocked,
this also affects the serial bit rate (which is presumably derived from
the peripheral clock). For this reason, serial communication with an
over- or under- clocked device will generally not work correctly, unless
the other device is similarly adjusted.

If transferring data between two overclocked Prizms, the transfer is
noticeably faster, but more prone to data transmission errors.
