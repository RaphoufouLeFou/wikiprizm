---
title: Technical Documentation
bookCollapseSection: true
aliases:
  - /Technical_Info/
---

Here you can find some specific information on certain qualities the
Prizm possesses, along with hardware specifications (the technical specs
of the device) and attributes.

-   [Hardware Revisions]({{< ref "Hardware_Revisions.md" >}})
-   [CPU Register
    List](https://docs.google.com/spreadsheet/ccc?key=0Ak-UVjh0rKHTdFJWMl9LN0c1WDVrZ1RkZ2pEcV9JQlE)
-   [CPU Clocks]({{< ref "CPU_Clocks.md" >}})
-   [Peripherals]({{< ref "Peripherals/_index.md" >}})
    -   Inside the CPU
        -   [Bus State Controller]({{< ref "Peripherals/Bus_State_Controller.md" >}})
        -   [Clock Pulse Generator]({{< ref "Peripherals/Clock_Pulse_Generator.md" >}})
        -   [Compare/Match Timer]({{< ref "Peripherals/Compare_Match_Timer.md" >}})
        -   [Direct Memory Access Controller]({{< ref "Peripherals/Direct_Memory_Access_Controller.md" >}})
        -   [Interrupt Controller]({{< ref "Peripherals/Interrupt_Controller.md" >}})
        -   [On-chip Memory]({{< ref "Peripherals/On-chip_Memory.md" >}})
        -   [Pin Function Controller]({{< ref "Peripherals/Pin_Function_Controller.md" >}})
        -   [RCLK Watchdog Timer]({{< ref "Peripherals/RCLK_Watchdog_Timer.md" >}})
        -   [Real-Time Clock]({{< ref "Peripherals/Real-Time_Clock.md" >}})
        -   [User Break Controller]({{< ref "Peripherals/User_Break_Controller.md" >}})
    -   Outside of the CPU
        -   [Flash]({{< ref "Flash.md" >}})
        -   [Display]({{< ref "Display.md" >}})
        -   [Serial]({{< ref "Serial.md" >}})
-   [File_System]({{< ref "File_System.md" >}})
-   [Bootloader]({{< ref "CASIOABS.md" >}})

## Basic Specifications {#basic_specifications}

-   **[Display]({{< ref "Technical_Documentation/Display.md" >}})**
    -   Type: Blanview LCD (Hyper Amorphous Silicon TFT), a Renesas
        R61524
    -   OS Resolution: 384×216 pixels
    -   Hardware Resolution: 396×224 pixels (**confirmed
        [here](http://www.cemetech.net/forum/viewtopic.php?p=180031#180031)**)
    -   OS "Homescreen" Resolution: 21×8 characters including top
        toolbar's space
-   **Processor**
    -   58MHz (overclockable within certain limits) Renesas SH4-A family
        SH7305 unit in big-endian mode
        -   RTC unit of a SH7720 base
        -   Lack of hardware FPU unit
-   **Memory**
    -   User ROM: 16 MiB
    -   Total ROM: 32 MiB
    -   User RAM: 61 KiB
    -   Total RAM: 2 MiB
-   **I/O**
    -   [USB port]({{< ref "USB_Communication.md" >}}): USB 2.0 mini-B
        (OS supports MSD mode for connection to a computer)
    -   [Serial port]({{< ref "Technical_Documentation/Serial.md" >}}):
        2.5 mm 3-pin
    -   [Keyboard]({{< ref "Keyboard.md" >}}): 50 key dome-switch
        keyboard + reboot pin-hole button on the back + internal OS
        update contacts
-   **Power**
    -   Max Consumption: 600mW
    -   Source: 4 AAA batteries (alkaline or Ni-MH)
    -   [Valid supply voltages, measuring supply
        voltage](http://www.cemetech.net/forum/viewtopic.php?t=8054)
-   **Physical Specs**
    -   Size: 2.06 cm × 8.95 cm × 18.85 cm / 0.81” × 3.52” × 7.42”
    -   Weight: 230 g / 0.51lb with batteries

    :   

## Pictures of the inside {#pictures_of_the_inside}

Here are links to some pictures of the inside (mostly just the main
board) of various Prizms.

-   flyingfisch's Prizms
    -   First fx-CG 10 (no visible hardware damage), hardware V02
        (really low-resolution, ignore the screw markings):
        [picture](http://s.lowendshare.com/1/1423611015.469.prizm-screw.png)
    -   Second broken fx-CG 10 (no visible hardware damage), hardware
        V03:
        [picture](http://s.lowendshare.com/10/1417207167.903.20140111_100655.jpg)
    -   Third broken fx-CG 10 (no visible hardware damage), hardware
        V03:
        [picture](http://s.lowendshare.com/10/1417207274.570.new%20calc%20(feb2014).jpg)
-   krazylegodrummer56 broken fx-CG 10 (no visible hardware damage),
    hardware V03:
    [bottom](http://s.lowendshare.com/10/1417207394.920.dgqyhc.jpg),
    [top](http://s.lowendshare.com/10/1417207421.707.jikfgn.jpg).
-   gbl08ma's repaired fx-CG 20: looks like a poor repair job from
    Casio's service center in a south European country. It was
    originally a fx-CG 20, died due to broken bootloader, was repaired
    under warranty and got its main board replaced with one of hardware
    version 04 **but** model fx-CG 10. The LCD controller is still the
    old version; the new main board also lacks the two big capacitors
    (they forgot to solder them to the new main board?). As of November
    2014, the calculator still worked fine. [picture
    1](http://s.lowendshare.com/10/1417207906.498.IMG_20131105_173543.jpg),
    [picture
    2](http://s.lowendshare.com/10/1417207932.238.IMG_20131105_173557.jpg),
    [picture
    3](http://s.lowendshare.com/10/1417207952.756.IMG_20131105_173608.jpg),
    [picture
    4](http://s.lowendshare.com/10/1417207975.949.IMG_20131105_173628.jpg).
