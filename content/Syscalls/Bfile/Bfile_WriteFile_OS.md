---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T11:26:36Z'
title: Bfile_WriteFile_OS
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DAF\
**Function signature:** int Bfile_WriteFile_OS(int handle, const void\*
buf, int size)

Writes to an open file at the current file position, given its handle,
the content to write and the size of that content.\

## Parameters

-   **handle** - the handle of the file to write (this is the value
    returned by
    [Bfile_OpenFile_OS]({{< ref "Syscalls/Bfile/Bfile_OpenFile_OS.md" >}})).
-   **buf** - pointer to buffer containing what should be written.
    Please note that this buffer should not be in the flash (it should
    not be `static` or `const`), because strange behavior has been
    observed when that is the case.
-   **size** - how many bytes to write. This can be the size of the
    buffer, or less if you don't want to write all the contents in the
    buffer.\

## Returns

If the operation succeeds, it returns the new file position. It is
greater than or equal to 0. If it fails, a negative error code is
returned.\

## Comments

**WARNING**: This function resizes the file if the content to write,
taking into account the current file position, does not fit in the
current [file
size]({{< ref "Syscalls/Bfile/Bfile_GetFileSize_OS.md" >}}). The file
cannot shrink, only expand. If you need to shrink a file,
[delete]({{< ref "Syscalls/Bfile/Bfile_DeleteEntry.md" >}}) then
[recreate]({{< ref "Syscalls/Bfile/Bfile_CreateEntry_OS.md" >}}) it, and
write the new contents.

The source buffer must not be in the flash, at least not in a file in
the storage memory, because during the write operation, automatic file
system optimization may make the source buffer get mapped to the wrong
address - situation similar to what causes the
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}}).

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.\

## Example

Here's an example that shows how to write a constant string to a file -
it must be copied to RAM first:


    // hFile is the handle of an open file
    char buffer[20]; // an array allocated on the stack
    strcpy(buffer, (char*)"Constant string");
    Bfile_WriteFile_OS(hFile, buffer, strlen(buffer));

Instead of providing the constant string to the syscall directly:


    Bfile_WriteFile_OS(hFile, "Constant string", 15); // may cause SYSTEM ERRORs, see Comments for an explanation
