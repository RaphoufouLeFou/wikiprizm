---
revisions:
- author: Gbl08ma
  timestamp: '2014-12-03T22:04:29Z'
title: OS_InnerWait_ms
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1BB4\
**Function signature:** void OS_InnerWait_ms(int ms)

Holds program execution for a given period of time.\

## Parameters

-   **ms** - time, in milliseconds, for which to hold program execution.
    Valid values range from 1 to 2000 ms.\

## Comments

According to Simon Lothar's documentation, this syscall works based on
the 7305 TMU (channel 2), which is a 7730 TMU with a different address.
