---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Cursor_SetFlashOff \\|\n\
    index = 0x08C8 \\| signature = void Cursor_SetFlashOff(void); \\|\nheader = fxcg/display.h\
    \ \\| synopsis = Stops the cursor, which\nposition is\u2026\u201D"
  timestamp: '2014-07-29T15:00:41Z'
title: Cursor_SetFlashOff
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x08C8\
**Function signature:** void Cursor_SetFlashOff(void);

Stops the cursor, which position is set using
[locate_OS]({{< ref "Syscalls/locate_OS.md" >}}), from displaying.
