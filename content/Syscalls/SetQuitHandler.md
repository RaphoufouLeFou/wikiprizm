---
revisions:
- author: Ahelper
  comment: Added VBR note
  timestamp: '2014-12-07T00:54:22Z'
title: SetQuitHandler
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1E6E\
**Function signature:** void SetQuitHandler(void (\*callback)())

Registers the handler that is called when the add-in terminates, that
is, gives its place to another child process. This allows for showing,
for instance, "Save file?" dialogs like the one shown when leaving
eActivity with a unsaved file open.\

## Parameters

-   **callback** - pointer to function that should be called when
    leaving the add-in.\

## Comments

Using a quit handler is only necessary if you are doing something
unusual on your add-in, that must be undone when leaving, or if you want
to ask the user something (but please, please do not this for annoying
"Really exit?" messages, because if the user is leaving, well, they really
want to).

Otherwise, there's no need to clean things up for the next application -
the OS takes care of resetting custom keyboard mappings,
[stack]({{< ref "/OS_Information/Processes.md#stack" >}}) and [heap]({{< ref "/OS_Information/Processes.md#heap" >}}) for
you, as well as stopping and uninstalling [user
timers]({{< ref "/OS_Information/Timers.md" >}}). [Bfile]({{< ref "Syscalls/Bfile/" >}})
handles and the VBR, however, are not automatically closed, and their
files will become unavailable for use by other add-ins if you don't
close them. Additionally, more problems can emerge on normal OS
operation due to a reduced amount of handles available, especially in
the case of file finding handles.
