---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  comment: Forgot category and rename link
  timestamp: '2015-02-11T12:36:30Z'
title: CMT
---

This is a list of syscalls for interfacing with the Prizm's
[Compare_Match_Timer]({{< ref "Compare_Match_Timer.md" >}}).
