---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T14:38:57Z'
title: MsgBoxMoveWB
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0938\
**Function signature:** void MsgBoxMoveWB(void\* buffer, int x0, int y0,
int x1, int y1, int direction)

Copies a rectangular area from VRAM to buffer, or restores it from
buffer to VRAM, automatically incrementing the vertical coordinate
values by 24 pixels, to account for the status bar.\

## Parameters

-   **buffer** - pointer to a buffer where to store (or load from) the
    VRAM contents. The buffer must be large enough to accommodate the
    data - if operating in full-color mode, it must have at least the
    amount of pixels to save times two (each pixel is two bytes). To
    help with the calculations, the buffer can be declared as an
    unsigned short array with size equal to the amount of pixels.
-   **x0**, **y0** - coordinates of the top-left point of the
    rectangular area. Note that y0 is incremented by 24 pixels by the
    syscall.
-   **x1**, **y1** - coordinates of the bottom-right point of the
    rectangular area. Note that y1 is incremented by 24 pixels by the
    syscall.
-   **direction** - if 1, copy from VRAM to buffer; if 0, copy from
    buffer to VRAM.
