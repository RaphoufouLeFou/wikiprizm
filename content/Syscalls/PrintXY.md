---
revisions:
- author: Dr-carlos
  comment: Re-arrange paramater list
  timestamp: '2022-03-26T03:06:34Z'
title: PrintXY
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x18F9\
**Function signature:** void PrintXY(int x, int y, char\* message, int
mode, int color)

Draws a line of homescreen-style text in VRAM, with the specified
position and color.\

## Parameters

-   **x** - The "homescreen" column of the first character to be drawn,
    1 to 20.
-   **y** - The "homescreen" row, 0 to 7 (see note 4).
-   **message** - pointer to a buffer (possibly `const` and/or `static`)
    containing the message to display. The first two characters of the
    message are ignored, see note 1.
-   **mode** - controls whether to clear text background, or invert
    background and foreground colors, using a bitmask. See note 2 for
    details.
-   **color** - text color of the characters, see note 3, below.\

## Comments

**Note 1**: The first two bytes are **not** ignored. The function of the
two bytes is still under study. It will always work to pad the beginning
of your string with characters which will be ignored as in the first set
of examples. You should not attempt to optimize your binary size a
little bit by subtracting two bytes from the actual string address.
Another reason why subtracting two from the string pointer is bad is
there may be certain rare situations where this scheme will cause a
crash (see note 5).

**Note 2**: Display mode 0 is normal, overwrite mode. The rectangle
containing each character is cleared. Setting bit 5 (0x20) makes the
background not be erased. Setting bit 0 (0x01) makes the text be written
in inverse mode. If you want both, bitwise OR the modes together.

You may also use the following, defined in *color.h*:

-   TEXT_MODE_NORMAL (0)
-   TEXT_MODE_INVERT (1)

When using 0x21 (0x20 \| 0x01), the effect isn't the expected where the
inverted text is drawn on top of the screen. Instead, it acts like an
AND operation where the printed text is used to mask what is currently
on the screen (color doesn't matter), printing white elsewhere.

**Note 3**: Valid colors are defined in *color.h* and are listed below:

-   TEXT_COLOR_BLACK (0) - Black
-   TEXT_COLOR_BLUE (1) - Blue
-   TEXT_COLOR_GREEN (2) - Green
-   TEXT_COLOR_CYAN (3) - Cyan
-   TEXT_COLOR_RED (4) - Red
-   TEXT_COLOR_PURPLE (5) - Purple
-   TEXT_COLOR_YELLOW (6) - Yellow
-   TEXT_COLOR_WHITE (7) - White

**Note 4**: Row 0:

A valid input to PrintXY's y parameter is 0, but this is usually not
rendered because row 0 is located under the status bar the top of the
screen. If the status bar is disabled, it will work perfectly.

**Note 5**: If the system attempts to read an invalid address you pass
to it as a parameter, this will fail. If your program is laid out in
memory such that your parameter is less than two bytes after an unmapped
memory region, reading the first two bytes will cause a fault. Given the
memory map for addon execution these situations should be very rare, but
for strict correctness always provide the leading bytes.

A sufficiently smart optimizer may also break code using this trick,
since out-of-bounds array access (including string literals) is
undefined behavior under the C standard. If the optimizer is able to
prove that your code accesses values outside the array bounds, it is
likely to remove it altogether (according to the standard it's permitted
to do literally anything, such as eating your laundry). In practice,
this has not yet been an issue.
