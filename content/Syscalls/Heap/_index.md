---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:33:52Z'
title: Heap
---

This is a list of [syscalls]({{< ref "syscalls.md" >}}) which manage the
[heap]({{< ref "/OS_Information/Processes.md#heap" >}}).

The heap implementation on the Prizm appears to have some bugs; it also
appears to not be able to optimize heap allocation so that fragmentation
is avoided. See [this forum
thread](http://www.cemetech.net/forum/viewtopic.php?t=7539) for more
information and discussion.

The [stack]({{< ref "/OS_Information/Processes.md#stack" >}}) can provide a bigger continuous RAM
area than the heap (up to about 500 KiB, depending on static RAM
requirements, versus about 128 KiB). This, plus the limitations
described above, means that to get the most out of the memory available
to add-ins, one has to use non-standard (*"incorrect"*) practices such
as preferably using the stack instead of the heap.
