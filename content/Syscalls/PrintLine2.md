---
revisions:
- author: Ahelper
  comment: libfxcg API
  timestamp: '2014-11-23T00:28:24Z'
title: PrintLine2
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1865\
**Function signature:** void PrintLine2(int P1, int mode, const char\*
msg, int P4, int P5, int imax, int color1, int color2)

Prints a message at the cursor position set with
[locate_OS]({{< ref "Syscalls/locate_OS.md" >}}) with custom back and
foreground colors.\

## Parameters

-   **P1** - unknown meaning.
-   **mode** - if bit 0 is set, the complete line is processed,
    otherwise only the message is processed. If bit 7 and bit 5 are set,
    **color2** is used as text color and **color1** is used as
    background color; else colors are inverted - **color1** is used as
    text color and **color2** is used as background color.
-   **msg** - pointer to a buffer (possibly `const` and/or `static`)
    containing the message to display.
-   **P4** - unknown meaning, set to 0.
-   **P5** - unknown meaning, set to -1.
-   **imax** - maximum length, zero-based, of the message to display.
    Characters past this length will not be displayed.
-   **color1** and **color2** - foreground and background colors for
    text (see [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}) for valid
    values).\

## Comments

[PrintLine]({{< ref "Syscalls/PrintLine.md" >}}) is a simplified version
of this syscall that does not support custom colors.
