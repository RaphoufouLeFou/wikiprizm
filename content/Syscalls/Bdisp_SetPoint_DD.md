---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T11:24:28Z'
title: Bdisp_SetPoint_DD
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x026B\
**Function signature:** void Bdisp_SetPoint_DD(int x, int y, unsigned
short color)

Sets a single pixel on the screen, bypassing VRAM.\

## Parameters

-   **x**: pixel column, 0 to 383.
-   **y**: pixel row, 0 to 215.
-   **color**: Pixel value to write.\

## Comments

You'll usually want to write to VRAM, rather than directly to the
display as this function does.
