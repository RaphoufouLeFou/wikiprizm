---
revisions:
- author: Gbl08ma
  comment: "Add descriptions for backgrounds which supposedly weren\u2019t\nused by\
    \ the OS, and that\u2019s not the case"
  timestamp: '2015-02-12T13:54:42Z'
title: SetBackGround
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1EF8\
**Function signature:** void SetBackGround(int background);

Draws to VRAM one of the built-in OS backgrounds.\

## Parameters

-   **background** - number of the background. See table below:

| background | output                                         |
|------------|------------------------------------------------|
| 0          | Matrix Mode Background                         |
| 1          | Equation Mode Background                       |
| 2          | eActivity Mode Background                      |
| 3          | Program Mode Background                        |
| 4          | Financial Mode Background                      |
| 5          | "Wakeup enable" background (Link mode)         |
| 6          | "Backup" background (Memory mode)              |
| 7          | System Mode Background                         |
| 8          | Language Background                            |
| 9          | "Search For Program" background (Program mode) |
| 10         | "New File" Background                          |
| 11         | Conversion Mode Background                     |
| 12         | "Open Spreadsheet" Background                  |
| 13         | "Save" Background (spreadsheet mode)           |
| 14         | "Capture Set Mode" background (Link Mode).     |
| 15         | Open geometry file background.                 |
| 16         | "Select cable type" background (Link Mode).    |
| 17         | same as 16.                                    |
| 18         | PC =\> Calc transfer background (link mode).   |
|            |                                                |
