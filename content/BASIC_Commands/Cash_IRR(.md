---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cash_IRR( = == Description == Thie\ncommand\
    \ returns the internal rate of return. == Syntax\n==\u2019\u2018\u2019Cash_IRR(\u2019\
    \u2019\u2019\u2018\u2019Cash\u2019\u2019\u2019\u2019\u2018)\u2019\u2019\u2019\
    \ == Example == Cash_IRR(300)\n\\[\\[Category:BASIC_Comm\u2026\u2019"
  timestamp: '2012-02-15T23:48:21Z'
title: Cash_IRR(
---

# Cash_IRR(

## Description

Thie command returns the internal rate of return.

## Syntax

**Cash_IRR(***Cash***)**

## Example

`Cash_IRR(300)`
