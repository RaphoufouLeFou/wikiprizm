---
revisions:
- author: Turiqwalrus
  comment: /\* Syntax \*/
  timestamp: '2012-02-29T18:06:28Z'
title: Circle
---

## Description

Draws a circle at the given location

## Syntax

`Circle <X-coordinate>,<Y-coordinate>,<radius in pixels>`

## Example

Draw a circle at (15,20) with radius 25.

```
Circle 15,20,25
```
