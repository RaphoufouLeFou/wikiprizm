---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= sinh = == Description == This command\nreturns\
    \ the hyperbolic sine of the value. == Syntax ==\u2019\u2018\u2019sinh\u2019\u2019\
    \u2019\n\u2018\u2019Value\u2019\u2019 == Example == sinh 2\u2019"
  timestamp: '2012-02-29T20:25:20Z'
title: Sinh
---

# sinh

## Description

This command returns the hyperbolic sine of the value.

## Syntax

**sinh** *Value*

## Example

`sinh 2`
