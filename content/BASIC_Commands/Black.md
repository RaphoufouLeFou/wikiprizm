---
title: Black
---

# Black

## Description

This command sets the subsequent Locate or other text command to render
in black.

## Syntax

**Black \<text command>**

## Example

`Black Locate 1, 1, "Asdf`
