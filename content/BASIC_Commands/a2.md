---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= a2 = == Description == This is the a2\nvalue\
    \ used in recursion. Can be used like the other variables. ==\nSyntax ==\u2019\
    \u2018\u2019a2\u2019\u2019\u2019 == Example == a2 ?\u2192a2 \\[\\[Category:BASIC_Comman\u2026\
    \u2019"
  timestamp: '2012-02-15T20:01:50Z'
title: a2
---

# a2

## Description

This is the a2 value used in recursion. Can be used like the other
variables.

## Syntax

**a2**

## Example

`a2`

`?→a2`
