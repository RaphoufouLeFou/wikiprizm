---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= CellProd( = == Description == This\ncommand\
    \ returns the product of the data in a specified cell range.\n== Syntax ==\u2019\
    \u2018\u2019CellProd(\u2019\u2019\u2019\u2018\u2019start_cell\u2019\u2018:\u2019\
    \u2018end_cell\u2019\u2019\u2019\u2019\u2018)\u2019\u2019\u2019 ==\nExampl\u2026\
    \u2019"
  timestamp: '2012-02-16T00:06:08Z'
title: CellProd(
---

# CellProd(

## Description

This command returns the product of the data in a specified cell range.

## Syntax

**CellProd(***start_cell*:*end_cell***)**

## Example

`CellProd(A3:C5)`
