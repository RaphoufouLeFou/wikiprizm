---
revisions:
- author: KermMartian
  timestamp: '2012-04-17T17:07:42Z'
title: Screen_Receiver
---

Community members BrandonW has been investigating the USB-connected
Screen Receiver software. He has discovered that there are at least two
types of screen images that the calculator will send, depending on
whether the calculator's LCD is in the 16-bit or 3-bit color mode. The
header looks like one of these two:

-   0x0B "TYPZ20000A20000D801801RC3" - 3-bit mode. In may contain
    byte-count 0x0A200 or packet count 0x0A2.
-   0x0B "TYPZ20002880000D801801RC2" - 16-bit mode. It may contain
    byte-count 0x28800 or packet count 0x288.

The contents of the packets sent, 0x100 (256) bytes at a time, are
two-byte pixels for the 16-bit mode, or nibble-packed pixels for the
3-bit mode. The 16-bit colors are 5R-6G-5B; the 3-bit colors are
1R-1G-1B-1trash.

## Resources

-   Source code for a Windows program to communicate with the Prizm in
    Screen Receiver mode can be found
    [here](http://brandonw.net/svn/x86stuff/USBMassStorage/trunk/)
